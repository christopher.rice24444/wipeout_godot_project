Credits:
- Programmers
	Christopher Rice
	Dani Amir
	Gabrial Cuneo
- Artist
	James Contreras

Description:
Basic 3D platformer built using the Godot game engine.
All scripts are programmed in C++ using the GDNative
module for Godot. The project features a water shader
for advanced water animations. The project also features
many player and obstacle interactions.

Building the Project:
After checking out the project with either

git clone git@gitlab.com:christopher.rice24444/wipeout_godot_project.git
or
git clone https://gitlab.com/christopher.rice24444/wipeout_godot_project.git

you should find all the necessary files to build the program. First we must build the CPP bindings.
Open your terminal of choice, and navigate to your project directory. Use the following commands to build the bindings:

cd godot-cpp
scons platform=<platform> generate_bindings=yes -j4 use_custom_api_file=yes custom_api_file=../api.json

where <platform> is the operating system you are using, either linux, osx, or windows.
Next use the following commands to build the project:

cd ..
scons platform=<platform>

where is once again the operating system you are using.